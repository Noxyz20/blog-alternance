---
title: "Mardi 28 Septembre 2021"
date: 2021-09-27T19:27:20+02:00
draft: false
---


Aujourd'hui j'ai mis en place un système de notifications sur le CMS  arosys. Assez simple avec laravel de base, mais sachant qu'on utilise vue3 avec InertiaJS il faut donc utiliser [Inertia::Share](https://inertiajs.com/shared-data) pour récupérer la donnée.

![toast](../images/toast.png)

J'ai donc créé une [middleware](https://laravel.com/docs/8.x/middleware) avec qui va permettre la réception du contenu de notre notification
```php
public  function  handle(Request  $request, Closure  $next)
{
	Inertia::share([
		'toast' => function() {
			return  Session::get('toast');
		}
	]);

    return  $next($request);
}
   ```
Pour passer notre notif il faut donc faire une redirection  laravel normal fléché de with()
```php
return  Redirect::route('user_management.index')->with(['toast' => ['message' => "User updated !"]]);
```
pour final tout récupérer dans un component  vuejs pour pouvoir l'insérer plus facilement dans notre layout !

```html
<template>
	<div  v-if="toast  &&  visible"  class="fixed flex items-center z-10 max-w-xs w-full mt-4 mr-4 top-0 right-0 bg-white rounded shadow p-4 ">
		<div  class="mr-2 text-green-600"><i  class="far fa-check-circle"></i></div>
		<div  class="flex-1">{{toast.message}}</div>
		<div  class="ml-2">
			<button
			class="text-gray-500 hover:text-gray-700 align-top focus:outline-none focus:text-yellow-600"
			@click="close()"
			>
				<i  class="fas fa-times"></i>
			</button>
		</div>
	</div>
</template>
```
<br>

Tech:
- Laravel 8.x
- VueJS 3
- InertiaJS
- TaillwindCSS