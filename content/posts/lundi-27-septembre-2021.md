---
title: "Lundi 27 Septembre 2021"
date: 2021-09-27T19:27:20+02:00
draft: false
---

Aujourd'hui c'est la reprise, donc petit skype café avec toute l'équipe et update de notre système linux ainsi que les dépendances d'arosys!

Après avoir vérifié et établie mon planning de la semaine, j'attaque avec le design de la galerie d'image côté front office :
![galerie index](../images/arosys_galerie.png)

J'ai aussi ajouté la possibilité de supprimer une galerie avec en cascade la suppression de toutes les images associer que ce soit dans la base de données et dans le storage.

```php
$pic  =  Galery::where('id', $id)->first();

foreach(Galery_pic::where('galery_id', $id)->get() as  $item) {
	unlink(public_path('images/'.$item->url));
}

$pic->delete();
```
On récupère d'abord la galerie qu'on stock dans une variable puis avec on utilise une boucle [foreach()](https://www.php.net/manual/fr/control-structures.foreach.php) avec en collection toutes les images appartenant à la galerie pour utiliser la fonction php [unlink()](https://www.php.net/manual/fr/function.unlink). `$pic->delete();` permet de supprimer la ligne dans une base de données.

Je me suis ensuite penché sur l'affichage côté client j'ai fini par trouver une petite solution de secours en attendant de creuser plus. Utiliser le moteur de rendu de laravel qui est [Blade](https://laravel.com/docs/8.x/blade#:~:text=Blade%20is%20the%20simple,%20yet,PHP%20code%20in%20your%20templates.&text=Blade%20template%20files%20use%20the,in%20the%20resources/views%20directory.). j'ai donc créé un fichier `galerie.blade.php` qui prend en paramètre l'id de la galerie.

```php
@php
use App\Models\Galery_pic;
use App\Models\Galery;
@endphp

<div  class="flex flex-row flex-wrap">
@foreach (Galery_pic::where('galery_id', $galerie_id)->get() as  $pic)
	<div  class="w-1/5 rounded hover:shadow-2xl">
		<img  class=""  src="/images/{{$pic->url}}">
	</div>
@endforeach
</div>
```
```php
@include('templates.gritaccess.galerie', ['galerie_id' => "1",])
```
<br>

Tech:
 - Laravel 8.x
 - VueJS 3
 - InertiaJS
 - TaillwindCSS

